package cbplus.testech.saveProducts.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import cbplus.testech.saveProducts.domain.models.Product
import org.koin.core.KoinComponent
import timber.log.Timber
import usecases.FetchProducts

class ProductsListViewModel(private val fetchProducts: FetchProducts) : ViewModel(), KoinComponent  {

    private var _result = MutableLiveData<List<Product>>()
    val result: LiveData<List<Product>>
        get() = _result

    init {
        loadProducts()
    }

    fun loadProducts() {
        Timber.d("I'm working in thread ${Thread.currentThread().name}")
        viewModelScope.launch {
            fetchProducts().collect { result ->
                _result.value = result
            }
        }
    }
}