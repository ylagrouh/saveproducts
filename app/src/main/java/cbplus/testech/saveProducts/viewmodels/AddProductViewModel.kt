package cbplus.testech.saveProducts.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cbplus.testech.saveProducts.domain.models.Product
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import timber.log.Timber
import usecases.SaveProduct

class AddProductViewModel(private val saveProduct: SaveProduct) : ViewModel(), KoinComponent {

    fun addProduct(product: Product) {
        Timber.d("I'm working in thread ${Thread.currentThread().name}")
        viewModelScope.launch {
            saveProduct(product)
        }
    }
}