package cbplus.testech.saveProducts.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import cbplus.testech.saveProducts.databinding.ProductItemBinding
import cbplus.testech.saveProducts.utils.ProductsDiffCallBack
import cbplus.testech.saveProducts.domain.models.Product
import org.koin.core.KoinComponent

class ProductsListAdapter : RecyclerView.Adapter<ProductsListAdapter.ViewHolder>(), KoinComponent
{

    private var products = mutableListOf<Product>()

    inner class ViewHolder(val binding: ProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
            fun bind(product: Product) {
                binding.product = product
                binding.executePendingBindings()
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ProductItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int =
        products.size


    fun updateProducts(newProducts: List<Product>) {
        val diffResult = DiffUtil.calculateDiff(ProductsDiffCallBack(products, newProducts))
        products.clear()
        products.addAll(newProducts)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int)
            = holder.bind(products[position])

}


