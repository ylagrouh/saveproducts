package cbplus.testech.saveProducts.utils

import androidx.recyclerview.widget.DiffUtil
import cbplus.testech.saveProducts.domain.models.Product

class ProductsDiffCallBack(private val oldProductList: List<Product>,
                           private val newProductList: List<Product>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldProductList[oldItemPosition].referenceNum == newProductList[newItemPosition].referenceNum

    override fun getOldListSize(): Int = oldProductList.size

    override fun getNewListSize(): Int = newProductList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldProductList[oldItemPosition].referenceNum == newProductList[newItemPosition].referenceNum
}