package cbplus.testech.saveProducts.utils

// swipe distance to sync
const val SWIPE_DISTANCE_TO_SYNC = 200

// fragment tags
const val ProductsListFragmentTag = "ProductsListFragment"