package cbplus.testech.saveProducts

import android.app.Application
import cbplus.testech.saveProducts.di.appModule
import di.dataModule
import di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class ProductsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (GlobalContext.getOrNull() == null) { // Used to prevent Robolectric to startKoin multiple times
            startKoin {
                if (BuildConfig.DEBUG) {
                    androidLogger(Level.DEBUG)
                } else {
                    androidLogger(Level.INFO)
                }
                androidContext(this@ProductsApplication)
                androidFileProperties()
                modules(listOf(appModule, dataModule, domainModule))
            }
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}