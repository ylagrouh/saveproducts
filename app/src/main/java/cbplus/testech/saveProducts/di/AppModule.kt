package cbplus.testech.saveProducts.di

import cbplus.testech.saveProducts.adapters.ProductsListAdapter
import cbplus.testech.saveProducts.viewmodels.AddProductViewModel
import cbplus.testech.saveProducts.viewmodels.MainViewModel
import cbplus.testech.saveProducts.viewmodels.ProductsListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single { ProductsListAdapter() }

    viewModel {
        MainViewModel()
    }

    viewModel {
        ProductsListViewModel(get())
    }

    viewModel {
        AddProductViewModel(get())
    }
}