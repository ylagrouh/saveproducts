package cbplus.testech.saveProducts.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import cbplus.testech.saveProducts.R
import cbplus.testech.saveProducts.domain.models.Product
import cbplus.testech.saveProducts.viewmodels.AddProductViewModel
import kotlinx.android.synthetic.main.fragment_add_product.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent
import java.util.*

class AddProductFragment : Fragment(), KoinComponent
{
    private val args : AddProductFragmentArgs by navArgs()
    private val addProductViewModel: AddProductViewModel by viewModel()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ref_num_tv.text = args.productReference
        setupListeners()

    }

    private fun setupListeners() {
        expiry_date_edt.setOnClickListener {
            showDatePicker()
        }

        save_btn.setOnClickListener {
            val expiryDate = expiry_date_edt.text.toString()
            val productReferenece = args.productReference
             if (expiryDate.isNotEmpty()) {
                 if (productReferenece.toLongOrNull() != null) {
                     val product = Product(productReferenece, expiry_date_edt.text.toString())
                     addProductViewModel.addProduct(product)
                     val action = AddProductFragmentDirections.actionAddProductFragmentToFragmentProductsList()
                     findNavController().navigate(action)
                 } else {
                     Toast.makeText(context, "Reference not valid", Toast.LENGTH_SHORT).show()
                 }
            } else {
                Toast.makeText(context, "Please enter expiry date to save product", Toast.LENGTH_SHORT).show()
            }
        }
        rescan_btn.setOnClickListener {
            val action = AddProductFragmentDirections.actionAddProductFragmentToScanProductFragment()
            findNavController().navigate(action)
        }
    }

    private fun showDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            val date = "$year-$monthOfYear-$dayOfMonth"
            expiry_date_edt.text =
                Editable.Factory.getInstance().newEditable(date)

        }, year, month, day)

        dpd.show()
    }




}