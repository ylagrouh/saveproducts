package cbplus.testech.saveProducts.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.util.isNotEmpty
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import cbplus.testech.saveProducts.R
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import kotlinx.android.synthetic.main.fragment_scan_product.*
import org.koin.core.KoinComponent

class ScanProductFragment : Fragment(), KoinComponent {

    private lateinit var detector: BarcodeDetector
    private lateinit var cameraSource: CameraSource

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_scan_product, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        settingUpCameraWithDetector()
    }

    private fun settingUpCameraWithDetector() {
        detector = BarcodeDetector.Builder(context).setBarcodeFormats(Barcode.ALL_FORMATS).build()
        detector.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {}

            override fun receiveDetections(detections: Detector.Detections<Barcode>?) {
                val barCodes = detections?.detectedItems
                if (barCodes != null && barCodes.isNotEmpty()) {
                    val referenceNumber = barCodes.valueAt(0).displayValue
                    val action = ScanProductFragmentDirections.actionScanProductFragmentToAddProductFragment(referenceNumber)
                    findNavController().navigate(action)
                }
            }
        })

        cameraSource = CameraSource.Builder(context, detector).setRequestedPreviewSize(1024, 768)
            .setRequestedFps(25f).setAutoFocusEnabled(true).build()

        sv_barcode.holder.addCallback(object : SurfaceHolder.Callback2 {
            override fun surfaceRedrawNeeded(holder: SurfaceHolder?) {
            }

            override fun surfaceChanged(
                holder: SurfaceHolder?,
                format: Int,
                width: Int,
                height: Int
            ) {}

            override fun surfaceDestroyed(holder: SurfaceHolder?) {
                cameraSource.stop()
            }

            override fun surfaceCreated(holder: SurfaceHolder?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkCamera()
                }else {
                    cameraSource.start(holder)
                }
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 123) {
            if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                cameraSource.start(sv_barcode.holder)
            } else {
                Toast.makeText(context, "Scanner won't work without permission.", Toast.LENGTH_SHORT)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkCamera() {
        with(requireContext()) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
                cameraSource.start(sv_barcode.holder)
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), 123)
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        detector.release()
        cameraSource.stop()
        cameraSource.release()
    }

}