package cbplus.testech.saveProducts.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import cbplus.testech.saveProducts.R
import cbplus.testech.saveProducts.adapters.ProductsListAdapter
import cbplus.testech.saveProducts.utils.SWIPE_DISTANCE_TO_SYNC
import cbplus.testech.saveProducts.viewmodels.ProductsListViewModel
import kotlinx.android.synthetic.main.fragment_products_list.*
import kotlinx.android.synthetic.main.fragment_products_list.view.*
import cbplus.testech.saveProducts.domain.models.Product
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent

class ProductsListFragment : Fragment(), KoinComponent {

    private val productsListViewModel: ProductsListViewModel by viewModel()
    private lateinit var adapter: ProductsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                             savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_products_list, container, false)
        initSwipeRefreshLayout(rootView)
        initProductsListAdapter(rootView)
        return rootView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ProductsListAdapter()
        initProductsObserver()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListners()
    }
    private fun initProductsObserver() {
        productsListViewModel.result.observe(this, Observer { result ->
            fg_products_srl.isRefreshing = false
            updateProducts(result)
        })
    }

    private fun clickListners() {
        scan_btn.setOnClickListener {
            val action =
                ProductsListFragmentDirections.actionProductListFragmentToScanProductFragment()
            findNavController().navigate(action)
        }
    }

    private fun initSwipeRefreshLayout(view: View?) {
        view?.fg_products_srl?.setOnRefreshListener {
            productsListViewModel.loadProducts()
            fg_products_srl.isRefreshing = true
        }
        view?.fg_products_srl?.setDistanceToTriggerSync(SWIPE_DISTANCE_TO_SYNC)
    }

    private fun initProductsListAdapter(view: View?) {
        view?.fg_products_rv?.adapter = adapter
        view?.fg_products_rv?.layoutManager = LinearLayoutManager(context)
    }

    private fun updateProducts(newProducts: List<Product>?) {
        if (newProducts != null) adapter.updateProducts(newProducts)
        fg_products_rv.scheduleLayoutAnimation()
    }
}