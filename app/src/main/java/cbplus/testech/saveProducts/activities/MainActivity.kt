package cbplus.testech.saveProducts.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import cbplus.testech.saveProducts.R
import cbplus.testech.saveProducts.viewmodels.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setNavHost()
    }

    private fun setNavHost() {
        val host = NavHostFragment.create(R.navigation.navigation_main_graph)
        supportFragmentManager.beginTransaction().replace(R.id.act_fragments_container, host)
            .setPrimaryNavigationFragment(host).commit()
    }
}
