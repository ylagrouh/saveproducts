package di

import cbplus.testech.saveProducts.db.AppDatabase
import mappers.ProductEntityMapper
import org.koin.core.qualifier.named
import org.koin.dsl.module
import sources.ProductLocalDataSource
import sources.ProductLocalDataSourceImpl

val dataModule = module {

    single { AppDatabase.getInstance(get()) }

    factory { ProductEntityMapper() }

    single(named("local")) { ProductLocalDataSourceImpl(get(), get()) as ProductLocalDataSource }

}