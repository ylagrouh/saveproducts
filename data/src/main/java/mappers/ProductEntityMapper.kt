package mappers

import cbplus.testech.saveProducts.db.entities.ProductEntity
import cbplus.testech.saveProducts.domain.models.Product
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.text.SimpleDateFormat

class ProductEntityMapper : Mapper<ProductEntity, Product> {

    private var format: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")


    override fun mapFromEntity(type: ProductEntity): Product  {
        val expiryDate = format.format(type.expireDate)
        return Product(type.referenceNum.toString(), expiryDate)
    }

    override fun mapToEntity(type: Product): ProductEntity  {
        val expiryDate = format.parse(type.expireDate)
        return ProductEntity(type.referenceNum.toLong(), expiryDate)
    }
}