package sources

import cbplus.testech.saveProducts.db.AppDatabase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import mappers.ProductEntityMapper
import cbplus.testech.saveProducts.domain.models.Product

class ProductLocalDataSourceImpl (private val mapper: ProductEntityMapper, private val db: AppDatabase) :
    ProductLocalDataSource {

    override fun getProducts(): Flow<List<Product>> =
        db.productDao().getProducts().map { list ->
            list.map {
                mapper.mapFromEntity(it)
            }
        }

    override suspend fun saveProduct(product: Product) =
        db.productDao().insert(mapper.mapToEntity(product))

}