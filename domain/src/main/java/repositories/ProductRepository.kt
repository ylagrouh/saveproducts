package repositories

import cbplus.testech.saveProducts.domain.models.Product
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import sources.ProductDataSource
import sources.ProductLocalDataSource

class ProductRepository(private val localSource: ProductLocalDataSource
) : ProductDataSource {

    override fun getProducts(): Flow<List<Product>> =
        localSource.getProducts()

    override suspend fun saveProduct(product: Product) = withContext(Dispatchers.IO) {
        localSource.saveProduct(product)
    }

}
