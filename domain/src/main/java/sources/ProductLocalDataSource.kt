package sources

import kotlinx.coroutines.flow.Flow
import cbplus.testech.saveProducts.domain.models.Product

interface ProductLocalDataSource {

    fun getProducts(): Flow<List<Product>>
    suspend fun saveProduct(product: Product)

}