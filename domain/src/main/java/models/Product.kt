package cbplus.testech.saveProducts.domain.models


data class Product(
    val referenceNum: String,
    val expireDate: String
)