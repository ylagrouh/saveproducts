package usecases

import cbplus.testech.saveProducts.domain.models.Product
import repositories.ProductRepository

class SaveProduct(private val productRepository: ProductRepository) {

    suspend operator fun invoke(product: Product) {
        productRepository.saveProduct(product)
    }
}
