package usecases

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import cbplus.testech.saveProducts.domain.models.Product
import org.slf4j.LoggerFactory
import repositories.ProductRepository

class FetchProducts(private val productRepository: ProductRepository) {

    private val logger = LoggerFactory.getLogger(FetchProducts::class.java.simpleName)

    @ExperimentalCoroutinesApi
    suspend operator fun invoke(): Flow<List<Product>> {
        logger.debug("Fetching products.")
        return productRepository.getProducts()
    }
}