package di

import org.koin.core.qualifier.named
import org.koin.dsl.module
import repositories.ProductRepository
import usecases.FetchProducts
import usecases.SaveProduct

val domainModule = module {

    single { ProductRepository(get(named("local"))) }

    factory { FetchProducts(get()) }

    factory { SaveProduct(get()) }
}