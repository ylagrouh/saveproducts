package cbplus.testech.saveProducts.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cbplus.testech.saveProducts.db.entities.ProductEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ProductDao {

    @Query("SELECT * FROM product_entity where referenceNum=:referenceNum")
    fun getById(referenceNum: Long): Flow<ProductEntity>

    @Query("SELECT * FROM product_entity")
    fun getProducts(): Flow<List<ProductEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(product: ProductEntity)

}