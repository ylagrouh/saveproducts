package cbplus.testech.saveProducts.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "product_entity")
data class ProductEntity (
    @PrimaryKey
    val referenceNum: Long,
    @ColumnInfo(name = "expire_date")
    val expireDate: Date

)
